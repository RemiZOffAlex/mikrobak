#!/usr/bin/env python3

__author__ = 'RemiZOffAlex'

import os

DEBUG = True
SQLDEBUG = False

DIR_BASE = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DIR_DATA = os.path.join(DIR_BASE, 'data')
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(DIR_DATA, 'db.sqlite3')
# Сегенерировать секретную фразу можно с помощью утилиты: pwgen -sy 64
SECRET_KEY = '''СЕКРЕТНАЯ-ФРАЗА'''

SESSION_COOKIE_NAME = 'entrypoint'
SESSION_TYPE = 'filesystem'

# Celery
# CELERY_BROKER_URL = 'db+sqlite:///' + DIR_DATA + '/task.db'
# CELERY_RESULT_BACKEND = 'db+sqlite:///' + DIR_DATA + '/results.db'
# CELERY_BROKER_URL = 'sqla+sqlite:///' + os.path.join(DIR_DATA, 'task.db')
# CELERY_RESULT_BACKEND = 'db+sqlite:///' + os.path.join(DIR_DATA, 'results.db')
CELERY_BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'

TITLE = "Хранение резервных копий MikroTik"

# Логирование
LOG_FILE = DIR_DATA + '/mikrobak.log'
LONG_LOG_FORMAT = '%(asctime)s - [%(name)s.%(levelname)s] [%(threadName)s, ' \
    '%(module)s.%(funcName)s@%(lineno)d] %(message)s'
LOG_FILE_SIZE = 128

# Количество выводимых элементов на странице
ITEMS_ON_PAGE = 100
