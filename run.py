#!/usr/bin/env python3

__author__ = 'RemiZOffAlex'

from mikrobak import app as application

application.run(debug = True, host= '0.0.0.0', port=5013)
