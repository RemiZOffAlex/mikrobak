__author__ = 'RemiZOffAlex'

from mikrobak import app
from flask import (
    Flask,
    render_template,
    request,
    escape,
    redirect,
    jsonify,
    abort
)

# RemiZOffAlex
from . import lib, models, forms
import difflib


"""
Постраничный вывод
"""
def getpage(query, page=1, page_size=10):
    if page_size:
        query = query.limit(page_size)
    if page:
        query = query.offset((page-1)*page_size)
    return query


@app.route('/', methods=['GET'])
def index():
    """Главная страница"""
    pagedata = {}
    pagedata['title'] = "Главная страница"
    body = render_template('index.html', pagedata=pagedata)
    return body


@app.route('/backups', defaults={'page': 1})
@app.route('/backups/<int:page>')
def backups(page):
    pagedata = {}
    pagedata['backups'] = models.db_session.query(models.Backup).order_by(models.Backup.created.desc())
    pagedata['pagination'] = lib.Pagination(
        page,
        app.config['ITEMS_ON_PAGE'],
        pagedata['backups'].count()
    )
    pagedata['pagination'].url = '/backups'
    pagedata['backups'] = getpage(
        pagedata['backups'],
        page,
        app.config['ITEMS_ON_PAGE']
    )
    pagedata['backups'] = pagedata['backups'].all()
    body = render_template('backups.html', pagedata=pagedata)
    return body


@app.route('/devices', defaults={'page': 1})
@app.route('/devices/<int:page>')
def devices(page):
    pagedata = {}
    pagedata['title'] = "Список устройств"
    pagedata['devices'] = models.db_session.query(
        models.Device
    ).order_by(
        models.Device.name
    )
    pagedata['pagination'] = lib.Pagination(
        page,
        app.config['ITEMS_ON_PAGE'],
        pagedata['devices'].count()
    )
    pagedata['pagination'].url = '/devices'
    pagedata['devices'] = lib.getpage(
        pagedata['devices'],
        page,
        app.config['ITEMS_ON_PAGE']
    )
    pagedata['devices'] = pagedata['devices'].all()
    body = render_template('devices.html', pagedata=pagedata)
    return body


@app.route('/diff', methods=['POST'])
def diffbackup():
    pagedata = {}
    baklist = request.form.getlist('backup')
    text1 = models.db_session.query(
        models.Backup
    ).filter(
        models.Backup.id==baklist[0]
    ).first()
    text2 = models.db_session.query(
        models.Backup
    ).filter(
        models.Backup.id==baklist[1]
    ).first()
    pagedata['backups'] = [text1, text2]
    pagedata['diff'] = '\n'.join(list(difflib.unified_diff(text1.text.split("\n"), text2.text.split("\n"))))
    body = render_template('diffview.html', pagedata=pagedata)
    return body


# noinspection PyUnusedLocal
@app.errorhandler(404)
def error_missing(exception):
    pagedata = {}
    error_message = "Не судьба..."
    return render_template("error.html", error_code=404, error_message=error_message, pagedata=pagedata), 404


# noinspection PyUnusedLocal
@app.errorhandler(403)
def error_unauthorized(exception):
    pagedata = {}
    error_message = "You are not authorized to view this page. Ensure you have the correct permissions."
    return render_template("error.html", error_code=403, error_message=error_message, pagedata=pagedata), 403


# noinspection PyUnusedLocal
@app.errorhandler(500)
def error_crash(exception):
    pagedata = {}
    error_message = "Вот незадача..."
    return render_template("error.html", error_code=500, error_message=error_message, pagedata=pagedata), 500
