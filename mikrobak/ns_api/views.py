__author__ = 'RemiZOffAlex'

import time
import datetime

from paramiko import SSHClient
from paramiko import AutoAddPolicy

from . import jsonrpc
from .. import models


@jsonrpc.method('device.getBackup')
def device_getBackup(id: int):
    """Получить /export
    """
    device = models.db_session.query(
        models.Device
    ).filter(
        models.Device.id==id
    ).first()
    if device is None:
        raise ValueError('Устройство не найдено')

    sshCli = SSHClient()
    sshCli.set_missing_host_key_policy(AutoAddPolicy())
    sshCli.connect(
        device.ip,
        port=22,
        username=device.username,
        password=device.password,
        look_for_keys=False,
        allow_agent=False,
        # compress = True
        timeout=10
    )
    stdin, stdout, stderr = sshCli.exec_command('/export')
    time.sleep(5)
    settings = stdout.read().decode('utf-8')
    error = stderr.read().decode('utf-8')
    sshCli.close()
    body = {
        'settings': settings,
        'error': error
    }
    return body
