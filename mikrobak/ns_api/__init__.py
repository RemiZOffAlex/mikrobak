__author__ = 'RemiZOffAlex'

from jsonrpc import JSONRPC
from functools import wraps

from .. import app, models

import datetime

from flask import session, redirect
from functools import wraps

jsonrpc = JSONRPC()

from . import (
    backup,
    views
)

import json
from flask import jsonify, request
from typing import Any

def to_json(request_data: bytes) -> Any:
    app.logger.info(request_data)
    try:
        return json.loads(request_data)
    except ValueError as e:
        app.logger.error('invalid json: %s', request_data)
        app.logger.exception(e)
        raise ParseError(data={'message': 'Invalid JSON: {0!r}'.format(request_data)})

@app.route('/api', methods=['POST'])
def api():
    json_data = to_json(request.data)
    result = jsonrpc(json_data)
    app.logger.info(result)
    app.logger.info(jsonify(result))
    return jsonify(result)
