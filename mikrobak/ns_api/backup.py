__author__ = 'RemiZOffAlex'

import time
import datetime

from . import jsonrpc
from .. import models


@jsonrpc.method('backup.delete')
def backup_delete(id: int) -> bool:
    backup = models.db_session.query(
        models.Backup
    ).filter(
        models.Backup.id==id
    ).first()
    if backup is None:
        raise ValueError('Backup not found')

    models.db_session.delete(backup)
    models.db_session.commit()

    return True
