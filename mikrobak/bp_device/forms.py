__author__ = 'RemiZOffAlex'

from wtforms import (
    validators,
    Form,
    BooleanField,
    StringField,
    TextAreaField,
    PasswordField,
    HiddenField,
    IntegerField,
    SelectField
)

class NewDeviceForm(Form):
    devicename = StringField('Имя')
    ip = StringField('IP')
    username = StringField('Пользователь')
    password = StringField('Пароль')
    sn = StringField('Серийный номер')

class EditDeviceForm(Form):
    devicename = StringField('Имя')
    ip = StringField('IP')
    username = StringField('Пользователь')
    password = StringField('Пароль')
    sn = StringField('Серийный номер')

class DeleteDeviceForm(Form):
    pass

class BackupForm(Form):
    title = StringField('Название')
    backuptext = TextAreaField('Настройки')

class BackupEdit(Form):
    title = StringField('Название')
    backuptext = TextAreaField('Настройки')
    comment = TextAreaField('Комментарии')
