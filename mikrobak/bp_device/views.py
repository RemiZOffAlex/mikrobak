__author__ = 'RemiZOffAlex'

import time
import datetime

from paramiko import SSHClient
from paramiko import AutoAddPolicy

from flask import (
    abort,
    render_template,
    request,
    escape,
    redirect,
    session,
    jsonify,
    Response
)

from . import bp_device, forms
from .. import app, lib, models


@bp_device.route('/add', methods=['GET', 'POST'])
def device_add():
    pagedata = {}
    pagedata['title'] = "Добавление нового устройства"
    pagedata['form'] = forms.NewDeviceForm(request.form)
    if request.method == 'POST':
        newdev = models.Device(name=escape(pagedata['form'].devicename.data),
            ip=escape(pagedata['form'].ip.data),
            username=escape(pagedata['form'].username.data),
            password=escape(pagedata['form'].password.data))
        if 'sn' in request.form.keys():
            newdev.serialnumber = escape(pagedata['form'].sn.data)
        models.db_session.add(newdev)
        models.db_session.commit()
        return redirect('/devices', code=302)
    body = render_template('device_add.html', pagedata=pagedata)
    return body


@bp_device.route('/<int:id>/edit', methods=['GET', 'POST'])
def device_edit(id):
    """Редактирование устройства"""
    pagedata = {}
    pagedata['device'] = models.db_session.query(
        models.Device
    ).filter(
        models.Device.id==id
    ).first()
    if pagedata['device'] is None:
        abort(404)
    pagedata['form'] = forms.EditDeviceForm(request.form,
        data={
            'devicename': pagedata['device'].name,
            'ip': pagedata['device'].ip,
            'username': pagedata['device'].username,
            'password': pagedata['device'].password,
            'sn': pagedata['device'].serialnumber
        }
    )
    if request.method == 'POST':
        if pagedata['form'].validate():
            pagedata['device'].name = escape(pagedata['form'].devicename.data)
            pagedata['device'].ip = escape(pagedata['form'].ip.data)
            pagedata['device'].username = escape(pagedata['form'].username.data)
            pagedata['device'].password = escape(pagedata['form'].password.data)
            models.db_session.commit()
            return redirect('/device/{}'.format(id), code=302)
    body = render_template('device_edit.html', pagedata=pagedata)
    return body


@bp_device.route('/<int:id>/delete', methods=['GET', 'POST'])
def device_delete(id):
    pagedata = {}
    pagedata['device'] = models.db_session.query(
        models.Device
    ).filter(
        models.Device.id==id
    ).first()
    if not pagedata['device']:
        abort(404)
    pagedata['form'] = forms.DeleteDeviceForm(request.form)
    if request.method == 'POST':
        for item in pagedata['device'].backups:
            models.db_session.delete(item)
            models.db_session.commit()
        models.db_session.delete(pagedata['device'])
        models.db_session.commit()
        return redirect('/devices', code=302)
    body = render_template('device_delete.html', pagedata=pagedata)
    return body


@bp_device.route('', defaults={'id': 0})
@bp_device.route('/<int:id>')
def device_id(id):
    pagedata = {}
    pagedata['device'] = models.db_session.query(models.Device).filter(models.Device.id == id).first()
    if pagedata['device']:
        pagedata['backups'] = models.db_session.query(models.Backup).filter(models.Backup.device_id == id).all()
    body = render_template('device.html', pagedata=pagedata)
    return body


@bp_device.route('/<int:id>/backup')
def device_backup(id):
    pagedata = {}
    pagedata['form'] = forms.BackupForm(request.form)
    pagedata['device'] = models.db_session.query(
        models.Device
    ).filter(
        models.Device.id==id
    ).first()
    body = render_template('backup.html', pagedata=pagedata)
    return body


@bp_device.route('/<int:id>/save', methods=['POST'])
def device_save(id):
    pagedata = {}
    device = models.db_session.query(
        models.Device
    ).filter(
        models.Device.id==id
    ).first()
    if device is None:
        abort(404)
    pagedata['form'] = forms.BackupForm(request.form)
    backup = models.Backup(device_id=id,
        title=escape(request.form['title']),
        text=escape(request.form['backuptext']))
    models.db_session.add(backup)
    models.db_session.commit()
    return redirect('/device/'+str(id), code=302)
