__author__ = 'RemiZOffAlex'

from wtforms import (
    validators,
    Form,
    BooleanField,
    StringField,
    TextAreaField,
    PasswordField,
    HiddenField,
    IntegerField,
    SelectField
)


class EditBackupForm(Form):
    title = StringField('Название')
    backuptext = TextAreaField('Настройки')
    comment = TextAreaField('Комментарии')


class UploadBackupForm(Form):
    ip = StringField('IP', default='192.168.88.1')
    username = StringField('Логин', default='admin')
    password = PasswordField('Пароль')
