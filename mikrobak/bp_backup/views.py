__author__ = 'RemiZOffAlex'

from flask import (
    abort,
    render_template,
    request,
    escape,
    redirect,
    session,
    Response
)

from . import bp_backup, forms
from .. import app, lib, models


@bp_backup.route('/<int:id>')
def backup_view(id):
    backup = models.db_session.query(
        models.Backup
    ).filter(
        models.Backup.id==id
    ).first()
    if backup is None:
        abort(404)

    pagedata = {}
    pagedata['backup'] = backup.as_dict()
    pagedata['backup']['device'] = backup.device.as_dict()

    body = render_template('backup_view.html', pagedata=pagedata)
    return body


@bp_backup.route('/<int:id>/delete')
def backup_delete(id):
    backup = models.db_session.query(
        models.Backup
    ).filter(
        models.Backup.id==id
    ).first()
    if backup is None:
        abort(404)

    pagedata = {}
    pagedata['backup'] = backup.as_dict()
    pagedata['backup']['device'] = backup.device.as_dict()

    body = render_template('backup_delete.html', pagedata=pagedata)
    return body


@bp_backup.route('/<int:id>/download')
def backup_download(id):
    backup = models.db_session.query(
        models.Backup
    ).filter(
        models.Backup.id==id
    ).first()
    if not backup:
        abort(404)
    return Response(backup.text, mimetype='text/plain')


@bp_backup.route('/<int:id>/edit', methods=['GET', 'POST'])
def backup_edit(id):
    pagedata = {}
    pagedata['backup'] = models.db_session.query(
        models.Backup
    ).filter(
        models.Backup.id==id
    ).first()
    if pagedata['backup'] is None:
        abort(404)
    pagedata['form'] = forms.EditBackupForm(
        request.form,
        data={
            'title': pagedata['backup'].title,
            'backuptext': pagedata['backup'].text,
            'comment': pagedata['backup'].comment
        }
    )
    if request.method=='POST':
        if pagedata['form'].validate():
            pagedata['backup'].title = escape(pagedata['form'].title.data)
            pagedata['backup'].text = escape(pagedata['form'].backuptext.data)
            pagedata['backup'].comment = escape(pagedata['form'].comment.data)
            models.db_session.commit()
            return redirect('/backup/' + str(id), code=302)
    body = render_template('backup_edit.html', pagedata=pagedata)
    return body


@bp_backup.route('/<int:id>/upload', methods=['GET', 'POST'])
def backup_upload(id):
    """Загрузить конфигурацию на устройство"""
    pagedata = {}
    pagedata['backup'] = models.db_session.query(
        models.Backup
    ).filter(
        models.Backup.id==id
    ).first()
    if pagedata['backup'] is None:
        abort(404)
    pagedata['form'] = forms.UploadBackupForm(request.form)
    if request.method == 'POST':
        if pagedata['backup']:
            pass
    body = render_template('upload.html', pagedata=pagedata)
    return body
