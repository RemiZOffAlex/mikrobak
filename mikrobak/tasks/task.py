__author__ = 'RemiZOffAlex'

from celery import group, chain

from mikrobak import app, celery, models


@celery.task()
def download(id):
    """
    Загрузка
    """
    device = models.db_session.query(
        models.Device
    ).filter(
        models.Device.id==id
    ).first()
    if not device:
        app.logger('Устройство с id=' + str(id) + ' не найдено')
        return None
    sshCli = SSHClient()
    sshCli.set_missing_host_key_policy(AutoAddPolicy())
    app.logger(pagedata['device'].ip)
    sshCli.connect(pagedata['device'].ip,
        port=22,
        username=pagedata['device'].username,
        password=pagedata['device'].password,
        look_for_keys=False)
    stdin, stdout, stderr = sshCli.exec_command('/export')
    time.sleep(25)
    sshCli.close()
    settings = stdout.read().decode('utf-8')
    app.logger(stderr.read().decode('utf-8'))
